import React from 'react';

function App() {
  return (
    <div className="cover-container d-flex h-100 p-3 mx-auto flex-column">
      <main role="main" className="inner cover">
        <h1 className="cover-heading">Welcome to Book App</h1>
        <p className="lead">Book App is a website that enables you to find your favorite books and get any references for you hobby</p>
      </main>

      <footer className="mastfoot mt-auto text-center">
        <div className="inner">
          <p>Made by Hafif | Copyright &copy; 2020.</p>
        </div>
      </footer>
    </div>
  );
}

export default App;
